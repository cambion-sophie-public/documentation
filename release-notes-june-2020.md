# Summary of Changes

This document summarizes changes to game mechanics released in June 2020.

## Age Groups

Vampire age groups have been reworked.
Age groups form the foundation for subsistence and bite strength.

| Title         | Age (Nights)
| --- | ---:
Fledgling          |    <30
Mature             |     30
Old                |    300
Venerable          |    500
Ancient            |  1,000
Colossus           |  1,500
Leviathan          |  2,000
Behemoth           |  2,500
Gorgonian          |  3,000
Promethean         |  3,500
Tartaran           |  4,000
Typhon             |  4,500
Draconian          |  5,000
Lomarian           |  6,000
Empyrean           |  7,500
Immortal           | 10,000
Eternal            | 15,000
Devourer of Worlds | 25,000

## Subsistence

Subsistence determines how much blood is lost each night due to being undead.
Thus, it governs how long a vampire can go without feeding.

Age group and outcast status determines a vampire's subsistence.
All vampires in the same age group have the same subsistence.

For outcasts, their subsistence is an additional 0.5 liter per night.

| Title         | Age (Nights) | Subsistence (liter/night) | Outcast Subsistence (liter/night)
| --- | ---: | ---: | ---:
Fledgling          |    <30  | 1.00 | 1.5
Mature             |     30  | 0.50 | 1.0
Old                |    300  | 0.45 | 0.95
Venerable          |    500  | 0.40 | 0.90
Ancient            |  1,000  | 0.35 | 0.85
Colossus           |  1,500  | 0.30 | 0.80
Leviathan          |  2,000  | 0.25 | 0.75
Behemoth           |  2,500  | 0.20 | 0.70
Gorgonian          |  3,000  | 0.10 | 0.60
Promethean         |  3,500  | 0.08 | 0.58
Tartaran           |  4,000  | 0.04 | 0.54
Typhon             |  4,500  | 0.02 | 0.52
Draconian          |  5,000  | 0.02 | 0.52
Lomarian           |  6,000  | 0.02 | 0.52
Empyrean           |  7,500  | 0.02 | 0.52
Immortal           | 10,000  | 0.02 | 0.52
Eternal            | 15,000  | 0.02 | 0.52
Devourer of Worlds | 25,000  | 0.01 | 0.51

## Bite Power

Bite power is controlled by age group.
Subsistence does not affect bite power in any way.

Vampires of a lower age group are at a disadvantage when biting a vampire of a higher age group.

## Gorging

Gorging allows a vampire to temporarily
hold more blood than their maximum.

Every 15 minutes,
part of the gorged blood is lost.

Age group determines the amount of gorged blood and the rate of loss.

| Title         | Age (Nights) | Normal Blood Maximum | Gorged Blood Maximum | Loss per 15 Minutes
| --- | ---: | ---: | ---: | ---:
Fledgling          |    <30  |  5.0 |   5.0 | 1.000
Mature             |     30  |  5.0 |   5.0 | 1.000
Old                |    300  |  5.0 |   6.0 | 0.125
Venerable          |    500  |  5.5 |   6.0 | 0.100
Ancient            |  1,000  |  6.0 |   7.0 | 0.125
Colossus           |  1,500  |  6.5 |   8.0 | 0.100
Leviathan          |  2,000  |  7.0 |   9.0 | 0.125
Behemoth           |  2,500  |  7.5 |  10.0 | 0.100
Gorgonian          |  3,000  |  8.0 |  11.1 | 0.375
Promethean         |  3,500  |  9.0 |  12.0 | 0.375
Tartaran           |  4,000  | 10.0 |  15.0 | 0.416
Typhon             |  4,500  | 15.0 |  20.0 | 0.416
Draconian          |  5,000  | 20.0 |  30.0 | 0.625
Lomarian           |  6,000  | 25.0 |  50.0 | 0.625
Empyrean           |  7,500  | 30.0 |  60.0 | 0.700
Immortal           | 10,000  | 35.0 |  70.0 | 0.700
Eternal            | 15,000  | 50.0 | 100.0 | 1.000
Devourer of Worlds | 25,000  | 60.0 | 200.0 | 1.500